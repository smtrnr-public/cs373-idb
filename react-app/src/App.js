import React, { Component } from 'react';
import { useEffect, useState } from 'react';
import './App.css';
import Favicon from 'react-favicon';

import AboutPage from "./Components/AboutPage";
import Albums from "./Components/Albums";
import Artists from "./Components/Artists";
import Tracks from "./Components/Tracks";
import ArtistInstance from "./Components/ArtistInstance";
import AlbumInstance from "./Components/AlbumInstance";
import TrackInstance from "./Components/TrackInstance";
import VoteApi from "./Components/VoteApi";
import SearchResults from "./Components/SearchResults";

import { Navbar, Nav, Button, Card, Form, FormControl, Table } from 'react-bootstrap';
import { BrowserRouter as Router,
  Switch,
  Route,
  useHistory
} from 'react-router-dom';

import { LinkContainer } from 'react-router-bootstrap';

//window.token == flask_token
//<p>My Token = {window.token}</p>
function App() {
    const [data, setData] = useState({artists: [], albums: [], tracks: [], search: false});
    const search = () => {
      const term = document.getElementById("search").value;
      fetch('/api/search?term=' + term).then(res => res.json()).then(data => {
        data["search"] = term != "";
        setData(data);
      });
    }
    let searchStyle = {
      verticalAlign: "middle",
      display: "block"
    };

    return (
      <div>
      <Favicon url="http://oflisback.github.io/react-favicon/public/img/github.ico" />
      <Router>
      <Navbar bg="warning" variant="light" expand="sm">
        <Navbar.Brand to="/">RockinwiththeRona</Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse>
            <Nav className="mr-auto">
                <LinkContainer to="/"><Nav.Link> Home </Nav.Link></LinkContainer>
                <LinkContainer to="/about"><Nav.Link> About </Nav.Link></LinkContainer>
                <LinkContainer to="/albums"><Nav.Link> Albums </Nav.Link></LinkContainer>
                <LinkContainer to="/artists"><Nav.Link> Artists </Nav.Link></LinkContainer>
                <LinkContainer to="/tracks"><Nav.Link> Tracks </Nav.Link></LinkContainer>
                <LinkContainer to="/voteApi"><Nav.Link> Vote With Knowledge </Nav.Link></LinkContainer>
            </Nav>
            <div style={searchStyle}>
              <label for="search">Search: </label>
              <input id="search" onKeyUp={() => search()}/>
            </div>
          </Navbar.Collapse>
      </Navbar>
      <div className="App">
          <br/><br/>
        </div>
        <Switch>
          <Route path="/voteApi" component={VoteApi} />
          <Route path="/artistInstance" component={ArtistInstance} />
          <Route path="/albumInstance" component={AlbumInstance} />
          <Route path="/trackInstance" component={TrackInstance} />
          <Route exact path="/about" component={AboutPage} />
          <Route path="/albums" component={Albums} />
          <Route path="/artists" component={Artists} />
          <Route path="/tracks" component={Tracks} />
          <Route path="/">
            <Splash data={data} />
          </Route>
        </Switch>
      </Router>
      </div>
    );
}

function Splash ( {data} ) {
  let display = <div></div>
  if (data["search"]){
    display = <SearchResults data={data} />
  } else {
    display = <div>
    <h1 class="text-center">Rockin' with the 'Rona</h1>
    <p class="text-center">A simple music database</p>
    <Card border = 'warning' bg = 'light' style={{ width: '18rem' }}>
      <Card.Body>
        <Card.Title>Welcome</Card.Title>
        <Card.Text>
          Click on a category above to browse artists, albums, or tracks, or search in the navigation bar to search all. 
        </Card.Text>
        
      </Card.Body>
    </Card>
    </div>
  }

  return (display);
}

export default App;