import React, { useEffect, useState } from "react";
import { Table } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';

function TrackInstance(props) {
    //const data = props.location.state.data;
    const history = useHistory();
    const routeChange = (pathName, rowData) =>{
      let path = pathName;
      history.push({
        pathname: path,
        state: { data: rowData},
      });
    }

    const [state, setState] = useState(props.location.state.data);
      useEffect(() => {
        fetch('/api/tracks?id=' + state.track_id).then(res => res.json()).then(data => {
          setState(data);
        });
      }, []);

    //<li><a href={item["artist_spotify_url"]}>{item["artist_name"]}</a></li>
    let artist_list = [];
    if ("track_artists" in state) {
        artist_list = state.track_artists.map(item => (
            <li style={{color: "blue"}} onClick={() => routeChange("/artistInstance", item)} >{item["artist_name"]}</li>
    ));
    }
    let albumContent = <p></p>
    if ("track_album" in state) {
        albumContent = <p style={{color: "blue"}} onClick={() => routeChange("/albumInstance", state.track_album)} >{state.track_album["album_name"]}</p>
    }
    return (
    <React.Fragment>
    <div>
        <a href={state.track_spotify_url}> <h1>{state.track_name}</h1> </a>
        <img src={state.track_image_url} style={{width:"200px"}} />
        <br />
        <h5>Release Date:</h5>
        <p>{state.track_release_day}</p>
    </div>
    <div style={{float: "left", width: "50%"}}>
        <h3>Album</h3>
        <ul>
            {albumContent}
        </ul>
    </div>
    <div style={{float: "right", width: "50%"}}>
        <h3>Artist(s)</h3>
        <ul>
            {artist_list}
        </ul>
    </div>
    </React.Fragment>
  );
}

export default TrackInstance;
