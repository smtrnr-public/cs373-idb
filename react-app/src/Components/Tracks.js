import React, { useEffect, useState } from "react";
//import { Table } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import MaterialTable from 'material-table';

function Tracks(props) {
  const history = useHistory();
  const routeChange = (pathName, rowData) =>{ 
    let path = pathName;
    history.push({
      pathname: path,
      state: { data: rowData},
    });
  }

  const [state, setState] = useState({
    columns: [
      { title: 'Name', field: 'track_name' },
      { title: 'Artist', field: 'track_artists[0].artist_name' },
      { title: 'Duration (Sec)', field: 'track_duration', type: 'numeric' },
      { title: 'Release Date', field: 'track_release_day'},
      { title: 'Spotify URL', field: 'track_spotify_url' },
    ],
    data: [],
  });
  useEffect(() => {
    fetch('/api/tracks?name=mic').then(res => res.json()).then(data => {
      setState({
        columns: [
          { title: 'Name', field: 'track_name' },
          { title: 'Artist', field: 'track_artists[0].artist_name' },
          { title: 'Duration (Sec)', field: 'track_duration', type: 'numeric'},
          { title: 'Release Date', field: 'track_release_day'},
          { title: 'Spotify URL', field: 'track_spotify_url' },
        ],
        data: data.Tracks,
      });
    });
  }, []);

    return (
    <React.Fragment>
    <MaterialTable
      title="Tracks"
      columns={state.columns}
      data={state.data}
      onRowClick={(event, rowData) => {
        routeChange("/trackInstance", rowData);
      }}
      editable={{
        onRowAdd: (newData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              setState((prevState) => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState((prevState) => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: (oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              setState((prevState) => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }}
    />
    </React.Fragment>
  );
}

export default Tracks;
