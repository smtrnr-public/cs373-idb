import React from "react";
import * as d3 from "d3";

const Arc = ({ data, index, createArc, colors }) => (
  <g key={index} className="arc">
    <path className="arc" d={createArc(data)} fill={colors} />
    <text
      transform={`translate(${createArc.centroid(data)})`}
      textAnchor="middle"
      alignmentBaseline="middle"
      fill="white"
      fontSize="15"
      fontWeight="bold"
    >
      {data.data.party + " (" + data.value + ")"}
    </text>
  </g>
);
const Pie = props => {
  const createPie = d3
    .pie()
    .value(d => d.value);
  const createArc = d3
    .arc()
    .innerRadius(props.innerRadius)
    .outerRadius(props.outerRadius);
  const data = createPie(props.data);

  return (
    <svg width={props.width} height={props.height}>
      <g transform={`translate(${props.outerRadius} ${props.outerRadius})`}>
        {data.map((d, i) => (
          <Arc
            key={i}
            data={d}
            index={i}
            createArc={createArc}
            colors={d.data.color}
          />
        ))}
        <text transform={`translate(0 ${props.height / 2})`} textAnchor="middle" alignmentBaseline="middle" fontSize="25">
          {props.title}
        </text>
      </g>
    </svg>
  );
};

export default Pie;