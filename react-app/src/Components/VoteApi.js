import React, { useEffect, useState } from "react";
import { Table } from 'react-bootstrap';
import Pie from "./Pie"
import Bar from "./Bar2"
import { format } from "d3";

function count(data) {
    let pieData = [{party: "Rep", value: 0, color: "red"}, {party: "Dem", value: 0, color: "blue"}, {party: "Ind", value: 0, color: "yellow"}];
    for (var i = 0; i < data.length; i++) {
        let mParty = data[i]["party"];
        if (mParty == "R") {
            pieData[0]["value"]++;
        } else if (mParty == "D") {
            pieData[1]["value"]++;
        } else {
            pieData[2]["value"]++;
        }
    }
    return pieData;
}

function randomColor() {
  let colors = "0123456789ABCDEF";
  let color = '#';
  for (var i = 0; i < 6; i++) {
    color += colors[Math.floor(Math.random() * 16)];
  }
  return color;
}

function VoteApi() {
    const [data, setData] = useState([]);
    const [billData, setBillData] = useState([]);
    const [stateData, setStateData] = useState([]);
    useEffect(() => {
      fetch('http://VoteWithKnowledge.me:5000/api/members').then(res => res.json()).then(data => {
        setData(data);
      });
      fetch('http://VoteWithKnowledge.me:5000/api/bills').then(res => res.json()).then(data => {
        setBillData(data);
      });
      fetch('http://VoteWithKnowledge.me:5000/api/states').then(res => res.json()).then(data => {
        setStateData(data);
      });
    }, []);

    let memberPieData = count(data);

    let billPieData = count(billData);
    let statePieData = [];
    let stateBarData = [];
    for (var i = 0; i < stateData.length; i++) {
      statePieData.push({party: stateData[i]["title"], value: (format(".2f")(stateData[i]["population"] / 1000000)), color: randomColor()});
      stateBarData.push({party: stateData[i]["abbr"], value: (stateData[i]["population"] / 1000000), color: randomColor()});
    }
    let barChart = <br />
    if (stateBarData.length > 0) {
      barChart = <Bar 
        width={1000}
        height={500}
        data={stateBarData}
      />
    }

    return (
        <React.Fragment>
        <p>Here we display statistics about voting from Group 4's API.</p>
        <div style={{textAlign: "center", marginBottom: "20px"}}>
          <div style={{display: "block", width: "50%", margin:"auto", float:"left"}}>
            <Pie
            data={memberPieData}
            width={400}
            height={600}
            innerRadius={0}
            outerRadius={200}
            title="Members"
            />
          </div>
          <div style={{display: "block", margin:"auto"}}>
            <Pie
            data={billPieData}
            width={400}
            height={600}
            innerRadius={0}
            outerRadius={200}
            title="Bills"
            />
          </div>
          <div style={{display: "block", width: "100%", margin:"auto"}}>
            <Pie
            data={statePieData}
            width={1000}
            height={1200}
            innerRadius={0}
            outerRadius={500}
            title="States"
            />
          </div>
          <div style={{display: "block", width: "100%", margin:"auto"}}>
            {barChart}
          </div>
        </div>
        </React.Fragment>
    )
}

export default VoteApi;
