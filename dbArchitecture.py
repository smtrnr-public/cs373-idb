from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# Change these to your PostgreSQL Database configuration
pUsername = "postgres"
pPassword = "abc123"
pHost = "localhost"
pPort = "5432"
pDB = "rockinwiththerona"

"""
Python file that can be imported to access database. Use the following line to import the necessary objects:

from dbArchitecture import db, Artist, Album, Track

Please run dbSetup.py before any import attempts of this Python file.
"""

# Connect SQLAlchemy to PostgreSQL
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "postgres://"+pUsername+":"+pPassword+"@"+pHost+":"+pPort+"/"+pDB
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True # Disables warnings
db = SQLAlchemy(app)

# Set up Many to Many relationship between artists and albums
artist_album = db.Table("artist_album",
    db.Column("artist_id", db.Integer, db.ForeignKey("artist.artist_id")),
    db.Column("album_id", db.Integer, db.ForeignKey("album.album_id"))
)

# Set up Many to Many relationship between artists and tracks
artist_track = db.Table("artist_track",
    db.Column("artist_id", db.Integer, db.ForeignKey("artist.artist_id")),
    db.Column("track_id", db.Integer, db.ForeignKey("track.track_id"))
)

# Create the classes for SQLAlchemy
class Artist(db.Model):
    """SQLAlchemy class for artists"""
    __tablename__ = "artist"
    artist_id = db.Column(db.Integer, primary_key=True)
    artist_name = db.Column(db.String)
    artist_albums = db.relationship("Album", secondary=artist_album, backref=db.backref("album_artists", lazy="dynamic"))
    artist_tracks = db.relationship("Track", secondary=artist_track, backref=db.backref("track_artists", lazy="dynamic"))
    artist_genres = db.Column(db.ARRAY(db.String))
    artist_image_url = db.Column(db.String)
    artist_spotify_url = db.Column(db.String)
    artist_spotify_id = db.Column(db.String)
    artist_hasinfo = db.Column(db.Boolean)

    def __init__(self, name, id, genres=[], image_url="", spotify_url="", spotify_id="", hasinfo=False):
        self.artist_id = id
        self.artist_name = name
        self.artist_genres = genres
        self.artist_image_url = image_url
        self.artist_spotify_url = spotify_url
        self.artist_spotify_id = spotify_id
        self.artist_hasinfo = hasinfo
    
    def __eq__(self, other):
        return self.artist_name == other.artist_name

class Album(db.Model):
    """SQLAlchemy class for albums"""
    __tablename__ = "album"
    album_id = db.Column(db.Integer, primary_key=True)
    album_name = db.Column(db.String)
    album_numtracks = db.Column(db.Integer)
    album_tracks = db.relationship("Track", backref="track_album")
    album_image_url = db.Column(db.String)
    album_release_day = db.Column(db.String)
    album_spotify_url = db.Column(db.String)
    album_spotify_id = db.Column(db.String)

    def __init__(self, name, id, numtracks=0, image_url="", release_day="", spotify_url="", spotify_id=""):
        self.album_id = id
        self.album_name = name
        self.album_numtracks = numtracks
        self.album_image_url = image_url
        self.album_release_day = release_day
        self.album_spotify_url = spotify_url
        self.album_spotify_id = spotify_id

class Track(db.Model):
    """SQLAlchemy class for tracks"""
    __tablename__ = "track"
    track_id = db.Column(db.Integer, primary_key=True)
    track_name = db.Column(db.String)
    track_duration = db.Column(db.Integer)
    track_album_id = db.Column(db.Integer, db.ForeignKey("album.album_id"))
    track_release_day = db.Column(db.String)
    track_image_url = db.Column(db.String)
    track_spotify_url = db.Column(db.String)
    track_preview_url = db.Column(db.String)
    track_spotify_id = db.Column(db.String)

# Commit to PostgreSQL Database
db.create_all()
db.session.commit()