from flask import Flask, render_template, request
from flask_cors import CORS, cross_origin
from dbArchitecture import db, Artist, Album, Track, app
import json
from sqlalchemy.dialects.postgresql import Any
import sys
import subprocess

CORS(app, resources={r"/api/*": {"origins": "*"}})

import sys
import subprocess

@app.route('/')
def index():
    """Home page"""
    return render_template('index.html', flask_token="Hello world")
"""flask_token doesn't do anything yet, but may be useful later on"""

# @app.route('/search', methods=['POST'])
# def search(): # Only requested from home page with a form (with method 'POST') response
#     """Search page"""
#     return render_template('search.html', term=request.form['search']) # term is the response from the form

def convert(data):
    if (hasattr(data, '__iter__')):
        result = []
        for x in data:
            curratedData = x.__dict__
            if '_sa_instance_state' in curratedData.keys():
                curratedData.pop('_sa_instance_state')
            result.append(curratedData)
        return result
    else:
        curratedData = data.__dict__
        if '_sa_instance_state' in curratedData.keys():
            curratedData.pop('_sa_instance_state')
        return curratedData


@app.route('/api/runtests') # should be a POST request but we can change that later
def runTests():
    out = subprocess.check_output([sys.executable, "tests.py"])
    return {"text": out.decode()}

@app.route('/api/artists')
def artistsApi():
    params = request.args
    if params.get('id'):
        id = params.get('id')
        artist = db.session.query(Artist).filter_by(artist_id = id).first()
        albums = convert(artist.artist_albums)
        tracks = convert(artist.artist_tracks)
        curratedData = convert(artist)
        curratedData['artist_albums'] = albums
        curratedData['artist_tracks'] = tracks
        result = json.dumps(curratedData)
    else:
        name = params.get('name')
        genre = params.get('genre')
        if (name and genre):
            artists = db.session.query(Artist).filter(Artist.artist_name.ilike("%" + name + "%"), Artist.artist_genres.any(genre))
        elif (name):
            artists = db.session.query(Artist).filter(Artist.artist_name.ilike("%" + name + "%"))
        elif (genre):
            artists = db.session.query(Artist).filter(Artist.artist_genres.any(genre))
        else:
            artists = db.session.query(Artist).all()
        result = []
        for artist in artists:
            albums = convert(artist.artist_albums)
            tracks = convert(artist.artist_tracks)
            curratedData = convert(artist)
            curratedData['artist_albums'] = albums
            curratedData['artist_tracks'] = tracks
            result.append(curratedData)
        result = json.dumps(result)
        result = "{ \"Artists\":" + result
        result += "}"
    return json.loads(result)

@app.route('/api/albums')
def albumsApi():
    params = request.args
    if params.get('id'):
        id = params.get('id')
        album = db.session.query(Album).filter_by(album_id = id).first()
        artists = convert(album.album_artists)
        tracks = convert(album.album_tracks)
        curratedData = convert(album)
        curratedData['album_artists'] = artists
        curratedData['album_tracks'] = tracks
        result = json.dumps(curratedData)
    else:
        name = params.get('name')
        date = params.get('date')
        if (name and date):
            albums = db.session.query(Album).filter(Album.album_name.ilike("%" + name + "%"), Album.album_release_day.ilike("%" + date + "%"))
        elif (name):
            albums = db.session.query(Album).filter(Album.album_name.ilike("%" + name + "%"))
        elif (date):
            albums = db.session.query(Album).filter(Album.album_release_day.ilike("%" + date + "%"))
        else:
            albums = db.session.query(Album).all()
        result = []
        for album in albums:
            artists = convert(album.album_artists)
            tracks = convert(album.album_tracks)
            curratedData = convert(album)
            curratedData['album_artists'] = artists
            curratedData['album_tracks'] = tracks
            result.append(curratedData)
        result = json.dumps(result)
        result = "{ \"Albums\":" + result
        result += "}"
    return json.loads(result)

@app.route('/api/tracks')
def tracksApi():
    params = request.args
    if params.get('id'):
        id = params.get('id')
        track = db.session.query(Track).filter_by(track_id = id).first()
        artists = convert(track.track_artists)
        albums = convert(track.track_album)
        curratedData = convert(track)
        curratedData['track_artists'] = artists
        curratedData['track_album'] = albums
        result = json.dumps(curratedData)
    else:
        name = params.get('name')
        date = params.get('date')
        if (name and date):
            tracks = db.session.query(Track).filter(Track.track_name.ilike("%" + name + "%"), Track.track_release_day.ilike("%" + date + "%"))
        elif (name):
            tracks = db.session.query(Track).filter(Track.track_name.ilike("%" + name + "%"))
        elif (date):
            tracks = db.session.query(Track).filter(Track.track_release_day.ilike("%" + date + "%"))
        else:
            tracks = db.session.query(Track).all()
        result = []
        for track in tracks:
            artists = convert(track.track_artists)
            albums = convert(track.track_album)
            curratedData = convert(track)
            curratedData['track_artists'] = artists
            curratedData['track_album'] = albums
            result.append(curratedData)
        result = json.dumps(result)
        result = "{ \"Tracks\":" + result
        result += "}"
    return json.loads(result)

@app.route('/api/search')
def searchApi():
    params = request.args
    searchJSON = {"artists": [], "albums": [], "tracks": []}
    if params.get('term'):
        searchterm = params.get('term')
        artistList = Artist.query.filter(Artist.artist_name.ilike("%" + searchterm + "%")).limit(50).all()
        albumList = Album.query.filter(Album.album_name.ilike("%" + searchterm + "%")).limit(50).all()
        trackList = Track.query.filter(Track.track_name.ilike("%" + searchterm + "%")).limit(50).all()
        for i in artistList:
            if i.artist_hasinfo:
                artistDict = {}
                artistDict["artist_id"] = i.artist_id
                artistDict["artist_name"] = i.artist_name
                artistDict["artist_genres"] = i.artist_genres
                artistDict["artist_image_url"] = i.artist_image_url
                artistDict["artist_spotify_url"] = i.artist_spotify_url
                searchJSON["artists"].append(artistDict)
        for i in albumList:
            albumDict = {}
            albumDict["album_id"] = i.album_id
            albumDict["album_name"] = i.album_name
            albumDict["album_numtrack"] = i.album_numtracks
            albumDict["album_image_url"] = i.album_image_url
            albumDict["album_spotify_url"] = i.album_spotify_url
            albumDict["album_artists"] = []
            for j in i.album_artists:
                tempDict = {}
                tempDict["artist_name"] = j.artist_name
                tempDict["artist_id"] = j.artist_id
                albumDict["album_artists"].append(tempDict)
            albumDict["album_release_day"] = i.album_release_day
            searchJSON["albums"].append(albumDict)
        for i in trackList:
            trackDict = {}
            trackDict["track_id"] = i.track_id
            trackDict["track_name"] = i.track_name
            trackDict["track_image_url"] = i.track_image_url
            trackDict["track_spotify_url"] = i.track_spotify_url
            trackDict["track_artists"] = []
            for j in i.track_artists:
                tempDict = {}
                tempDict["artist_name"] = j.artist_name
                tempDict["artist_id"] = j.artist_id
                trackDict["track_artists"].append(tempDict)
            tempDict = {}
            tempDict["album_name"] = i.track_album.album_name
            tempDict["album_id"] = i.track_album.album_id
            trackDict["track_album"] = tempDict
            trackDict["track_release_day"] = i.track_release_day
            searchJSON["tracks"].append(trackDict)
    return searchJSON

@app.route('/tests')
def tests():
    return subprocess.check_output([sys.executable, "tests.py"])

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port = '80')
    #app.run(debug=True)
